package com.tixget.app.tixget;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.flexbox.FlexboxLayout;
import com.tixget.app.tixget.base.User;
import com.tixget.app.tixget.function.ImgBit;
import com.tixget.app.tixget.function.cUrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SearchActivity extends AppCompatActivity {
    private User user;
    LayoutInflater inflater;
    FlexboxLayout  flexboxLayout;
    Context mCtx = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        final Bundle base = getIntent().getExtras();

        TextView name = (TextView)findViewById(R.id.name);
        name.setText(base.getString("name"));
        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        flexboxLayout = (FlexboxLayout)findViewById(R.id.context);
        inflater = LayoutInflater.from(this);
        user = new User(this);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest sr = new StringRequest(Request.Method.GET,"https://api.tixget.com/"+user.getLng()+"/search/?un=RockStar&pa=Um9ja1N0YXI=&fcy="+user.getFcy()+"&search="+base.getString("name"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    final JSONArray array = json.getJSONArray("items");
                    Log.i("test", array.toString());
                    for(int x = 0; x < array.length(); x++){
                        View view = inflater.inflate(R.layout.frag_categories, flexboxLayout, false);
                        CardView cardView = view.findViewById(R.id.card);
                        ImageView imageView = view.findViewById(R.id.img);
                        TextView city = view.findViewById(R.id.City);
                        TextView textView = view.findViewById(R.id.name);
                        TextView textViewe = view.findViewById(R.id.normal_price);
                        TextView price = view.findViewById(R.id.price);

                        textView.setText(array.getJSONObject(x).getString("name"));
                        price.setText(array.getJSONObject(x).getJSONObject("price").getString("price"));
                        textViewe.setText(array.getJSONObject(x).getJSONObject("price").getString("normal_price"));
                        city.setText("");
                        textViewe.setPaintFlags(textViewe.getPaintFlags() | Paint.DEV_KERN_TEXT_FLAG);

                        new ImgBit(imageView).execute(array.getJSONObject(x).getString("thumbnail"));
                        flexboxLayout.addView(view);
                        final int finalX = x;
                        cardView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle b = new Bundle();
                                try {
                                    b.putString("base", array.getJSONObject(finalX).getString("id"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Intent instant = new Intent(SearchActivity.this, AttractionActivity.class);
                                instant.putExtras(b);
                                startActivity(instant);
                                cUrl cUrl = new cUrl(mCtx);
                                try {
                                    cUrl.getAttraction(mCtx,"https://api.tixget.com/"+user.getLng()+"/attraction/"+array.getJSONObject(finalX).getString("id")+"/?un=RockStar&pa=Um9ja1N0YXI=&fcy=thb"+user.getFcy(), array.getJSONObject(finalX).getString("id"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        sr.setShouldCache(false);
        queue.add(sr);
    }
}
