package com.tixget.app.tixget.base;

import android.content.ContentValues;

public class popular {
    public static final String DATABASE_NAME = "base_popular.db";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE = "popular";

    private int id;
    private String name;
    private String thumbnail;
    private String normal_price;
    private String price;
    private String fcy;
    private ContentValues base;
    public popular() {

    }
    public class Column {
        public static final String ID_attc = "id_attc";
        public static final String Name = "name";
        public static final String normal_price = "normal_price";
        public static final String price = "price";
        public static final String thumbnail = "thumbnail";
        public static final String fcy = "fcy";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setFirstName(String name) {
        this.name = name;
    }

    public String getthumbnail() {
        return thumbnail;
    }

    public void setthumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getnormal_price() {
        return normal_price;
    }

    public void setnormal_price(String normal_price) {
        this.normal_price = normal_price;
    }

    public String getprice() {
        return price;
    }

    public void setprice(String price) {
        this.price = price;
    }

    public ContentValues getBase() {
        return base;
    }

    public void setBase(ContentValues base) {
        this.base = base;
    }
}
