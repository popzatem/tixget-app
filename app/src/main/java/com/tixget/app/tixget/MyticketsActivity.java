package com.tixget.app.tixget;

import android.content.Context;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tixget.app.tixget.base.User;
import com.tixget.app.tixget.function.ImgBit;
import com.tixget.app.tixget.function.function;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyticketsActivity extends AppCompatActivity {
    private Context mCtx = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mytickets);
        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        final LinearLayout content = (LinearLayout)findViewById(R.id.content);
        final LayoutInflater inflater = LayoutInflater.from(this);
        User user = new User(this);
        RequestQueue queue = Volley.newRequestQueue(mCtx);
        StringRequest sr = new StringRequest(Request.Method.GET,"https://api.tixget.com/en/bookinginfo/"+user.getID()+"/?un=RockStar&pa=Um9ja1N0YXI=&nocache=1", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    if("200".equals(json.getString("status"))){
                        final JSONArray item = json.getJSONArray("items");
                        for(int x = 0; x < item.length(); x++){
                            if("16".equals(item.getJSONObject(x).getString("status"))){
                                View view = inflater.inflate(R.layout.list_mytickets, content, false);
                                TextView name = view.findViewById(R.id.name);
                                TextView time = view.findViewById(R.id.time);
                                ImageView img = view.findViewById(R.id.img);
                                name.setText(item.getJSONObject(x).getString("name"));
                                time.setText(item.getJSONObject(x).getString("bought_date"));
                                new ImgBit(img).execute(item.getJSONObject(x).getString("thumbnail"));
                                final int finalX = x;
                                view.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent instant = new Intent(MyticketsActivity.this, TicketsActivity.class);
                                        Bundle b = new Bundle();
                                        try {
                                            b.putString("refno", function.MD5(item.getJSONObject(finalX).getString("refno")));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        instant.putExtras(b);
                                        startActivity(instant);
                                    }
                                });
                                content.addView(view);
                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        sr.setShouldCache(false);
        queue.add(sr);
    }
}
