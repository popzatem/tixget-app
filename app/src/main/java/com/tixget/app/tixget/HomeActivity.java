package com.tixget.app.tixget;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tixget.app.tixget.base.User;
import com.tixget.app.tixget.function.DBHelper;
import com.tixget.app.tixget.function.ImgBit;
import com.tixget.app.tixget.function.cUrl;
import com.tixget.app.tixget.function.function;
import com.tixget.app.tixget.function.Page;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static android.support.v4.view.GravityCompat.*;

public class HomeActivity extends AppCompatActivity {
    private TextView onclick_all,onclick_custom,onclick_nearby,test;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private NavigationView nvView;
    private Context mCtx = this;
    private User user;
    private ImageView close,menu,imageView11;
    private DecimalFormat formatter;
    private RequestQueue queue;
    private View view;
    ArrayList<String> mylist = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        user = new User(mCtx);
        final Page page = new Page("all",null);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_view);
        function.removeNavigationShiftMode(bottomNavigationView);
        formatter = new DecimalFormat("#,###,###,###");
        menu = (ImageView) findViewById(R.id.menu);
        imageView11 = (ImageView) findViewById(R.id.imageView11);
        queue = Volley.newRequestQueue(mCtx);
        StringRequest sr = new StringRequest(Request.Method.GET,"https://api.tixget.com/"+user.getLng()+"/autocompletefull/?un=RockStar&pa=Um9ja1N0YXI=", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONArray array = null;
                try {
                    array = new JSONArray(response);

                    for(int x = 0; x< array.length(); x++){
                        mylist.add(array.getJSONObject(x).getString("name"));
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mCtx, R.layout.select_dialog_item, mylist);
                    final AutoCompleteTextView search = (AutoCompleteTextView) findViewById(R.id.search);
                    search.setThreshold(1);
                    search.setAdapter(adapter);
                    search.setTextColor(Color.BLACK);
                    search.setOnEditorActionListener(new TextView.OnEditorActionListener() {

                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (event != null&& (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            }
                            Intent instant = new Intent(HomeActivity.this, SearchActivity.class);
                            Bundle b = new Bundle();
                            b.putString("name", search.getText().toString());
                            instant.putExtras(b);
                            startActivity(instant);
                            search.setText("");
                            return false;
                        }
                    });
                    search.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick (AdapterView<?> parent, View view, int position, long id) {
                            Intent instant = new Intent(HomeActivity.this, SearchActivity.class);
                            Bundle b = new Bundle();
                            b.putString("name", search.getText().toString());
                            instant.putExtras(b);
                            startActivity(instant);
                            search.setText("");
                        }
                    });
                    search.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if(count == 1){
                                search.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                                menu.setVisibility(View.GONE);
                                imageView11.setVisibility(View.GONE);
                            }else{
                                search.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT));
                                menu.setVisibility(View.VISIBLE);
                                imageView11.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        queue.add(sr);



        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_recent:
                        // do this event
                        return true;
                    case R.id.item_favorite:
                        // do this event
                        return true;
                    case R.id.item_nearby:
                        // do this event
                        return true;
                    case R.id.item_test:
                        startActivity(new Intent(HomeActivity.this,ProfileActivity.class));
                        return true;
                }
                return false;
            }
        });

        onclick_all = (TextView)findViewById(R.id.onclick_custon_all);
        onclick_custom = (TextView)findViewById(R.id.onclick_custom_home);
        onclick_nearby = (TextView)findViewById(R.id.onclick_custon_nearby);
        onclick_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page.setPage("all");
                ViewPage(page);
            }
        });
        onclick_custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page.setPage("custom");
                ViewPage(page);
            }
        });
        ViewPage(page);
        MenuHamburger();
    }
    private void MenuHamburger (){
        ImageView icon_lng;
        TextView icon_fcy;
        LinearLayout mLng,mFcy;
        mDrawerLayout = (DrawerLayout) findViewById(R.id.mDrawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        nvView = (NavigationView)findViewById(R.id.nvView);
        LayoutInflater inflater = LayoutInflater.from(this);
        View headerView = inflater.inflate(R.layout.nav_header, nvView, false);
        icon_lng = headerView.findViewById(R.id.icon_lng);
        icon_fcy = headerView.findViewById(R.id.icon_fcy);
        close = headerView.findViewById(R.id.exit);
        mLng = headerView.findViewById(R.id.mLng);
        mFcy = headerView.findViewById(R.id.mFcy);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        });
        mFcy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this,CurrencyActivity.class));
            }
        });
        mLng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this,LanguageActivity.class));
            }
        });
        if(user.getFcy().equals("thb")){
            icon_fcy.setText("฿");
        }else if(user.getFcy().equals("usd")){
            icon_fcy.setText("$");
        }else if(user.getFcy().equals("cny")){
            icon_fcy.setText("¥");
        }

        if(user.getLng().equals("th")){
            icon_lng.setImageResource(R.drawable.icon_th);
        }else if(user.getLng().equals("en")){
            icon_lng.setImageResource(R.drawable.icon_en);
        }else if(user.getLng().equals("cn")){
            icon_lng.setImageResource(R.drawable.icon_cn);
        }

        nvView.addView(headerView);
        nvView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch(id)
                {
                    case R.id.nav_logout:
                        user.Signout();
                        Intent intent = new Intent(HomeActivity.this,SigninActivity.class);
                        startActivity(intent);
                    default:
                        return true;
                }
            }
        });
    }
    private void ViewPage (Page page){
        final Context mCtx = this;
        final LinearLayout context = (LinearLayout)findViewById(R.id.context);
        final LayoutInflater inflater = LayoutInflater.from(this);
        user = new User(mCtx);
        Log.i("CPage", page.getPage() + " / target : " + page.getTarget());
        if(!"all".equals(page.getTarget()) && page.getPage().equals("all")){
            onclick_all.setPaintFlags(onclick_all.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            onclick_custom.setPaintFlags(onclick_custom.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
            onclick_nearby.setPaintFlags(onclick_nearby.getPaintFlags() & (~Paint.UNDERLINE_TEXT_FLAG));
            context.removeAllViews();
            DBHelper mHelper = new DBHelper(this);
            final Cursor base = mHelper.queryAll("popular");
            view = inflater.inflate(R.layout.form_popular_home, context, false);

//----------------------------------------------------------------------------------------------
            queue = Volley.newRequestQueue(mCtx);
            StringRequest Api_popular = new StringRequest(Request.Method.GET,"http://ec2-13-228-251-38.ap-southeast-1.compute.amazonaws.com:3000/popular/"+user.getLng(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    JSONArray array = null;
                    try {
                        array = new JSONArray(response);
                        context.addView(view);
                        LinearLayout list2 = (LinearLayout)view.findViewById(R.id.list2);
                        for(int x = 0; x< array.length(); x++){
                            mylist.add(array.getJSONObject(x).getString("name"));
                            View view2 = inflater.inflate(R.layout.list_popular_home, list2, false);
                            final TextView textView = view2.findViewById(R.id.name);
                            TextView textViewe = view2.findViewById(R.id.normal_price);
                            TextView price = view2.findViewById(R.id.price);
                            CardView cardView = view2.findViewById(R.id.bglayout);
                            ImageView imageView = view2.findViewById(R.id.PopularIMG);
                            ImageView sharing = view2.findViewById(R.id.sharing);
                            RatingBar rating = view2.findViewById(R.id.rating2);
                            textViewe.setPaintFlags(textViewe.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            if(!array.getJSONObject(x).getJSONObject("price").getString("price").equals("null")){
                                price.setText(function.LangCode(user.getFcy())+" "+formatter.format(Integer.parseInt(array.getJSONObject(x).getJSONObject("price").getString("price"))));
                            }
                            if(!array.getJSONObject(x).getJSONObject("price").getString("normal_price").equals("-") && !array.getJSONObject(x).getJSONObject("price").getString("normal_price").equals("null") ){
                                textViewe.setText(function.LangCode(user.getFcy())+" "+formatter.format(Integer.parseInt(array.getJSONObject(x).getJSONObject("price").getString("normal_price"))));
                            }
                            final String Id = array.getJSONObject(x).getString("id");
                            sharing.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String name = (String) textView.getText();
                                    String link = new String("https://www.tixget.com/home/"+user.getLng()+"/detail/"+Id+"/"+name.replace(' ','-')+"/?media=&fcy="+user.getFcy());
                                    Intent sendIntent = new Intent();
                                    sendIntent.setAction(Intent.ACTION_SEND);
                                    sendIntent.putExtra(Intent.EXTRA_TEXT, link);
                                    sendIntent.setType("text/plain");
                                    startActivity(sendIntent);
                                }
                            });
                            textView.setText(array.getJSONObject(x).getString("name")); // id = 0 | 1 = normal_price | 2 = img | 3 = price | name = 4 | fcy = 5
                            new ImgBit(imageView).execute(array.getJSONObject(x).getString("thumbnail"));
                            list2.addView(view2);
                            final String res = array.getJSONObject(x).getString("id");
                            cardView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Bundle b = new Bundle();
                                    b.putString("base", res);
                                    Intent instant = new Intent(HomeActivity.this, AttractionActivity.class);
                                    instant.putExtras(b);
                                    startActivity(instant);
                                    cUrl cUrl = new cUrl(mCtx);
                                    cUrl.getAttraction(mCtx,"https://api.tixget.com/"+user.getLng()+"/attraction/"+res+"/?un=RockStar&pa=Um9ja1N0YXI=&fcy=thb", res);
                                }
                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            queue.add(Api_popular);
//----------------------------------------------------------------------------------------------

            final LinearLayout list_cty = (LinearLayout)view.findViewById(R.id.list_cty);
            queue = Volley.newRequestQueue(mCtx);
            StringRequest sr = new StringRequest(Request.Method.GET,"http://ec2-13-228-251-38.ap-southeast-1.compute.amazonaws.com:3000/categories/"+user.getLng(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    JSONArray array = null;
                    JSONObject json = null;
                    try {
                        array = new JSONArray(response);
                        int loop = (int) Math.ceil((double)array.length()/6);
                        int mun = 0;
                        for(int x = 0; x < loop; x++){
                            TextView text1,text2,text3,text4,text5,text6;
                            View cty_list = inflater.inflate(R.layout.list_categories_home, list_cty, false);
                            text1 = cty_list.findViewById(R.id.text1);
                            text2 = cty_list.findViewById(R.id.text2);
                            text3 = cty_list.findViewById(R.id.text3);
                            text4 = cty_list.findViewById(R.id.text4);
                            text5 = cty_list.findViewById(R.id.text5);
                            text6 = cty_list.findViewById(R.id.text6);
                            ImageView cardView = cty_list.findViewById(R.id.card1);
                            ImageView cardView2 = cty_list.findViewById(R.id.card2);
                            ImageView cardView3 = cty_list.findViewById(R.id.card3);
                            ImageView cardView4 = cty_list.findViewById(R.id.card4);
                            ImageView cardView5 = cty_list.findViewById(R.id.card5);
                            ImageView cardView6 = cty_list.findViewById(R.id.card6);

                            try {
                                if(!array.getJSONObject(mun).isNull("name")){
                                    text1.setText(array.getJSONObject(mun).getString("name"));
                                    new ImgBit(cardView).execute("https://files.tixget.com/attractions/thumb_horizontal/20180122150544889.jpg");
                                    final int finalMun = mun;
                                    final JSONArray finalArray = array;
                                    cardView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent instant = new Intent(HomeActivity.this, CategoriesActivity.class);
                                            Bundle b = new Bundle();
                                            try {
                                                b.putString("name", finalArray.getJSONObject(finalMun).getString("name"));
                                                b.putString("id", finalArray.getJSONObject(finalMun).getString("id"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            instant.putExtras(b);
                                            startActivity(instant);
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if(!array.getJSONObject((1+mun)).isNull("name")){
                                    text2.setText(array.getJSONObject((1+mun)).getString("name"));
                                    new ImgBit(cardView2).execute("https://files.tixget.com/attractions/thumb_horizontal/201806051023526845.jpg");
                                    final int finalMun = (1+mun);
                                    final JSONArray finalArray = array;
                                    cardView2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent instant = new Intent(HomeActivity.this, CategoriesActivity.class);
                                            Bundle b = new Bundle();
                                            try {
                                                b.putString("name", finalArray.getJSONObject(finalMun).getString("name"));
                                                b.putString("id", finalArray.getJSONObject(finalMun).getString("id"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            instant.putExtras(b);
                                            startActivity(instant);
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if(!array.getJSONObject((2+mun)).isNull("name")){
                                    new ImgBit(cardView3).execute("https://files.tixget.com/attractions/thumb_horizontal/201801041429039751.png");
                                    text3.setText(array.getJSONObject((mun+2)).getString("name"));
                                    final int finalMun = (2+mun);
                                    final JSONArray finalArray = array;
                                    cardView3.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent instant = new Intent(HomeActivity.this, CategoriesActivity.class);
                                            Bundle b = new Bundle();
                                            try {
                                                b.putString("name", finalArray.getJSONObject(finalMun).getString("name"));
                                                b.putString("id", finalArray.getJSONObject(finalMun).getString("id"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            instant.putExtras(b);
                                            startActivity(instant);
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if(!array.getJSONObject((3+mun)).isNull("name")){
                                    new ImgBit(cardView4).execute("https://files.tixget.com/attractions/thumb_horizontal/201801250830587719.jpg");
                                    text4.setText(array.getJSONObject((3+mun)).getString("name"));
                                    final int finalMun = (3+mun);
                                    final JSONArray finalArray = array;
                                    cardView4.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent instant = new Intent(HomeActivity.this, CategoriesActivity.class);
                                            Bundle b = new Bundle();
                                            try {
                                                b.putString("name", finalArray.getJSONObject(finalMun).getString("name"));
                                                b.putString("id", finalArray.getJSONObject(finalMun).getString("id"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            instant.putExtras(b);
                                            startActivity(instant);
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if(!array.getJSONObject((4+mun)).isNull("name")){
                                    new ImgBit(cardView5).execute("https://files.tixget.com/attractions/thumb_horizontal/201801091702438650.png");
                                    text5.setText(array.getJSONObject((4+mun)).getString("name"));
                                    final int finalMun = (4+mun);
                                    final JSONArray finalArray = array;
                                    cardView5.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent instant = new Intent(HomeActivity.this, CategoriesActivity.class);
                                            Bundle b = new Bundle();
                                            try {
                                                b.putString("name", finalArray.getJSONObject(finalMun).getString("name"));
                                                b.putString("id", finalArray.getJSONObject(finalMun).getString("id"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            instant.putExtras(b);
                                            startActivity(instant);
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if(!array.getJSONObject((5+mun)).isNull("name")){
                                    new ImgBit(cardView6).execute("https://files.tixget.com/attractions/thumb_horizontal/20180319033326746.jpg");
                                    text6.setText(array.getJSONObject((5+mun)).getString("name"));
                                    final int finalMun = (5+mun);
                                    final JSONArray finalArray = array;
                                    cardView6.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent instant = new Intent(HomeActivity.this, CategoriesActivity.class);
                                            Bundle b = new Bundle();
                                            try {
                                                b.putString("name", finalArray.getJSONObject(finalMun).getString("name"));
                                                b.putString("id", finalArray.getJSONObject(finalMun).getString("id"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            instant.putExtras(b);
                                            startActivity(instant);
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            list_cty.addView(cty_list);
                            mun = mun+6;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            queue.add(sr);

            page.setTarget("all");
        }else if(!"custom".equals(page.getTarget()) && page.getPage().equals("custom")){
            onclick_custom.setPaintFlags(onclick_custom.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            onclick_all.setPaintFlags(onclick_all.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
            onclick_nearby.setPaintFlags(onclick_nearby.getPaintFlags() & (~Paint.UNDERLINE_TEXT_FLAG));
            context.removeAllViews();
            View view = inflater.inflate(R.layout.form_custom_trip, context, false);
            context.addView(view);
            page.setTarget("custom");
        }
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("ยืนยัน");
        builder.setMessage("ต้องการออกจากระบบใช่หรือไม่ค่ะ ?");

        builder.setPositiveButton("ออกจากระบบ", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                //  Exit Application
                moveTaskToBack(true);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
