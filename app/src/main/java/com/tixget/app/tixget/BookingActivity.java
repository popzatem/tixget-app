package com.tixget.app.tixget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.tixget.app.tixget.api.Attraction_api;
import com.tixget.app.tixget.base.ticket_sum;
import com.tixget.app.tixget.function.ImgBit;
import com.tixget.app.tixget.function.function;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.function.Predicate;

public class BookingActivity extends AppCompatActivity {
    private String stringTime;
    private int stringMONTH;
    private MaterialCalendarView widget;
    private EditText promocode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        widget = (MaterialCalendarView)findViewById(R.id.calendarView2);
        final Context mCtx = this;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final Bundle base = getIntent().getExtras();
                Attraction_api attraction_api = new Attraction_api(mCtx);
                Cursor data = attraction_api.getID(base.getString("base"));

                final DecimalFormat formatter = new DecimalFormat("#,###,###,###");
                final ticket_sum ticket = new ticket_sum();
                final int[] total = {0};
                if (data.moveToFirst()){
                    try {

                        final JSONObject database = new JSONObject(data.getString(1));
                        Log.i("res-attc", database.toString());

                        SimpleDateFormat df = new SimpleDateFormat("dd");
                        final Calendar min = Calendar.getInstance();
                        int Datemin = 0;
                        if("4".equals(database.getString("category_id"))){
                            Datemin = (Integer.parseInt(df.format(min.getTime()))+1);
                        }else{
                            Datemin = Integer.parseInt(df.format(min.getTime()));
                        }
                        Toast.makeText(getApplicationContext(), String.valueOf(Datemin), 0).show();
                        min.set(Calendar.DATE, Datemin);
                        min.set(Calendar.HOUR_OF_DAY, 0);
                        long startOfMonth = min.getTimeInMillis();
                        Calendar max = Calendar.getInstance();
                        widget.setSelectedDate(min);
                        widget.state().edit()
                                .setMinimumDate(min)
                                .commit();
                        ArrayList<CalendarDay> enabledDates = new ArrayList<>();
                        for(int x = 0; x <  database.getJSONArray("attractions_special_close_date").length(); x++){
                            StringTokenizer st = new StringTokenizer(database.getJSONArray("attractions_special_close_date").getString(x), "-");
                            String year = st.nextToken();
                            String mon = st.nextToken();
                            String day = st.nextToken();
                            enabledDates.add(new CalendarDay(Integer.parseInt(year), (Integer.parseInt(mon)-1), Integer.parseInt(day)));
                        }

                        widget.addDecorator(new PrimeDayDisableDecorator(enabledDates));
                        stringMONTH = min.get(Calendar.MONTH)+ 1;
                        stringTime = String.format("%02d", stringMONTH)+"/"+String.format("%02d", min.get(Calendar.DAY_OF_MONTH))+"/"+min.get(Calendar.YEAR);
                        widget.setOnDateChangedListener(new OnDateSelectedListener() {
                            @Override
                            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                                stringMONTH = date.getMonth()+ 1;
                                stringTime = String.format("%02d", stringMONTH)+"/"+String.format("%02d", date.getDay())+"/"+String.valueOf(date.getYear());
                                Toast.makeText(getApplicationContext(), stringTime, 0).show();
                            }
                        });

                        promocode = (EditText)findViewById(R.id.ePromocode);



                        TextView aName = (TextView)findViewById(R.id.aName);
                        aName.setText(database.getString("name"));
                        LinearLayout tixget = (LinearLayout)findViewById(R.id.list_tixget);
                        LayoutInflater inflater = LayoutInflater.from(mCtx);
                        final TextView txtTotal = (TextView) findViewById(R.id.total);
                        final ArrayList<List<String>> list = new ArrayList<>();
                        final ArrayList<String> lists = new ArrayList<String>();
                        final ArrayList<String> tk = new ArrayList<String>();
                        list.clear();
                        final TextView [] number = new TextView[99];
                        for (int x = 0 ; x < database.getJSONArray("price_list").length(); x++){
                            View view = inflater.inflate(R.layout.list_tixget_booking, tixget, false);
                            TextView textView = view.findViewById(R.id.name);
                            TextView price = view.findViewById(R.id.price);
                            TextView mPrice = view.findViewById(R.id.mPrice);
                            number[x] = view.findViewById(R.id.number);
                            ImageView plus = view.findViewById(R.id.plus);
                            ImageView delete = view.findViewById(R.id.delete);
                            mPrice.setPaintFlags(mPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            mPrice.setText(database.getJSONArray("price_list").getJSONObject(x).getString("normal_price"));
                            list.add(Arrays.asList(base.getString("base"), database.getJSONArray("price_list").getJSONObject(x).getString("id"), "0", database.getJSONArray("price_list").getJSONObject(x).getString("name")));
                            lists.add(base.getString("base")+"|"+database.getJSONArray("price_list").getJSONObject(x).getString("id")+"|0|"+database.getJSONArray("price_list").getJSONObject(x).getString("name"));
                            Log.i("test",list.get(x).get(0).toString());
                            final int Idticket = Integer.parseInt(base.getString("base"));
                            price.setText(function.LangCode(database.getJSONArray("price_list").getJSONObject(x).getString("fcy"))+" "+formatter.format(Integer.parseInt(database.getJSONArray("price_list").getJSONObject(x).getString("price"))));
                            textView.setText(database.getJSONArray("price_list").getJSONObject(x).getString("name"));
                            final int finalX = x;
                            plus.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    int p = Integer.parseInt(list.get(finalX).get(2))+1;
                                    try {
                                        list.set(finalX, Arrays.asList(base.getString("base"), database.getJSONArray("price_list").getJSONObject(finalX).getString("id"), ""+p, database.getJSONArray("price_list").getJSONObject(finalX).getString("name")));
                                        number[finalX].setText(String.valueOf(p));
                                        lists.set(finalX, base.getString("base")+"|"+database.getJSONArray("price_list").getJSONObject(finalX).getString("id")+"|"+p+"|"+database.getJSONArray("price_list").getJSONObject(finalX).getString("name"));
                                        tk.add(database.getJSONArray("price_list").getJSONObject(finalX).getString("id"));
                                        Log.i("test",lists.get(finalX).toString());
                                        Log.i("test",tk.toString());
                                        total[0] = (total[0] + Integer.parseInt(database.getJSONArray("price_list").getJSONObject(finalX).getString("price")));
                                        txtTotal.setText(String.valueOf(formatter.format(total[0])));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            delete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if(Integer.parseInt(list.get(finalX).get(2)) >= 1){
                                        int p = Integer.parseInt(list.get(finalX).get(2))-1;
                                        try {
                                            list.set(finalX, Arrays.asList(base.getString("base"), database.getJSONArray("price_list").getJSONObject(finalX).getString("id"), ""+p, database.getJSONArray("price_list").getJSONObject(finalX).getString("name")));
                                            number[finalX].setText(String.valueOf(p));
                                            lists.set(finalX, base.getString("base")+"|"+database.getJSONArray("price_list").getJSONObject(finalX).getString("id")+"|"+p+"|"+database.getJSONArray("price_list").getJSONObject(finalX).getString("name"));
                                            Log.i("test",lists.get(finalX).toString());
                                            total[0] = (total[0] - Integer.parseInt(database.getJSONArray("price_list").getJSONObject(finalX).getString("price")));
                                            Iterator<String> it = tk.iterator();
                                            it.hasNext();
                                            if (it.next().contains(database.getJSONArray("price_list").getJSONObject(finalX).getString("id"))){
                                                it.remove();
                                            }
                                            Log.i("test",tk.toString());
                                            txtTotal.setText(String.valueOf(formatter.format(total[0])));


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                            tixget.addView(view);
                            Log.i("res-attc", database.getJSONArray("price_list").getJSONObject(x).getString("id"));

                        }
                        Button booking = (Button)findViewById(R.id.Booking);
                        booking.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle b = new Bundle();
                                ArrayList<List<String>> lists = new ArrayList<>(list);
                                b.putString("id", base.getString("base"));
                                try {
                                    b.putString("name", database.getString("name"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                b.putString("date", stringTime);
                                b.putString("total", String.valueOf(total[0]));
                                Intent instant = new Intent(BookingActivity.this, PaymentActivity.class);
                                instant.putExtra("list", list);
                                instant.putExtra("booking", tk);
                                instant.putExtras(b);
                                startActivity(instant);
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                       finish();
                    }
                    while(data.moveToNext());
                }
            }
        },1000);
    }
    private static class PrimeDayDisableDecorator implements DayViewDecorator {

        private HashSet<CalendarDay> dates;

        public PrimeDayDisableDecorator(Collection<CalendarDay> dates) {
            this.dates = new HashSet<>(dates);
        }

        @Override public boolean shouldDecorate(final CalendarDay day) {
            return dates.contains(day);
        }

        @Override public void decorate(final DayViewFacade view) {
            view.setDaysDisabled(true);
        }
    }

    private static class EnableOneToTenDecorator implements DayViewDecorator {

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return day.getDay() <= 10;
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.setDaysDisabled(false);
        }
    }
    public class DayEnableDecorator implements DayViewDecorator {
        private HashSet<CalendarDay> dates;

        public DayEnableDecorator(Collection<CalendarDay> dates) {
            this.dates = new HashSet<>(dates);
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return dates.contains(day);
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.setDaysDisabled(false);
        }
    }
}
