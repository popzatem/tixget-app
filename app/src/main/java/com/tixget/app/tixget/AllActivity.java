package com.tixget.app.tixget;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tixget.app.tixget.function.DBHelper;
import com.tixget.app.tixget.function.ImgBit;
import com.tixget.app.tixget.function.Page;
import com.tixget.app.tixget.function.cUrl;
import com.tixget.app.tixget.function.function;

public class AllActivity extends AppCompatActivity {
    private TextView onclick_all,onclick_custom,onclick_nearby;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        final Page page = new Page("all",null);


        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_view);
        function.removeNavigationShiftMode(bottomNavigationView);
        EditText search = (EditText) findViewById(R.id.search);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_recent:
                        // do this event
                        return true;
                    case R.id.item_favorite:
                        // do this event
                        return true;
                    case R.id.item_nearby:
                        // do this event
                        return true;
                    case R.id.item_test:
                        // do this event
                        return true;
                }
                return false;
            }
        });

        onclick_all = (TextView)findViewById(R.id.onclick_custon_all);
        onclick_custom = (TextView)findViewById(R.id.onclick_custom_home);
        onclick_nearby = (TextView)findViewById(R.id.onclick_custon_nearby);
        onclick_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page.setPage("all");
                ViewPage(page);
            }
        });
        onclick_custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page.setPage("custom");
                ViewPage(page);
            }
        });
        ViewPage(page);
    }
    private void ViewPage (Page page){
        final Context mCtx = this;
        final LinearLayout context = (LinearLayout)findViewById(R.id.context);
        LayoutInflater inflater = LayoutInflater.from(this);
        Log.i("CPage", page.getPage() + " / target : " + page.getTarget());
        if(!"all".equals(page.getTarget()) && page.getPage().equals("all")){
            onclick_all.setPaintFlags(onclick_all.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            onclick_custom.setPaintFlags(onclick_custom.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
            onclick_nearby.setPaintFlags(onclick_nearby.getPaintFlags() & (~Paint.UNDERLINE_TEXT_FLAG));
            context.removeAllViews();
            DBHelper mHelper = new DBHelper(this);
            Cursor base = mHelper.queryAll("popular");

            View view = inflater.inflate(R.layout.form_popular_home, context, false);
            context.addView(view);
            LinearLayout list2 = (LinearLayout)view.findViewById(R.id.list2);

            while(!base.isAfterLast()) {
                View view2 = inflater.inflate(R.layout.list_popular_home, list2, false);
                TextView textView = view2.findViewById(R.id.name);
                TextView textViewe = view2.findViewById(R.id.normal_price);
                TextView price = view2.findViewById(R.id.price);
                CardView cardView = view2.findViewById(R.id.bglayout);
                ImageView imageView = view2.findViewById(R.id.PopularIMG);
                textViewe.setPaintFlags(textViewe.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                if(!base.getString(3).equals("null")){
                    price.setText(function.LangCode(base.getString(5))+" "+base.getString(3));
                }
                if(!base.getString(1).equals("-") && !base.getString(1).equals("null") ){
                    textViewe.setText(function.LangCode(base.getString(5))+" "+base.getString(1));
                }
                textView.setText(base.getString(4)); // id = 0 | 1 = normal_price | 2 = img | 3 = price | name = 4 | fcy = 5
                new ImgBit(imageView).execute(base.getString(2));
                list2.addView(view2);
                final String res = base.getString(0);
                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle b = new Bundle();
                        b.putString("base", res);
                        Intent instant = new Intent(AllActivity.this, AttractionActivity.class);
                        instant.putExtras(b);
                        startActivity(instant);
                        cUrl cUrl = new cUrl(mCtx);
                        cUrl.getAttraction(mCtx,"https://api.tixget.com/en/attraction/"+res+"/?un=RockStar&pa=Um9ja1N0YXI=&fcy=thb", res);
                    }
                });
                Log.i("res", String.valueOf(base.getInt(0)));
                base.moveToNext();
            }



            LinearLayout list_cty = (LinearLayout)view.findViewById(R.id.list_cty);
            View cty_list = inflater.inflate(R.layout.list_categories_home, list_cty, false);
            ImageView cardView = cty_list.findViewById(R.id.card1);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(AllActivity.this,CategoriesActivity.class));
                }
            });
            ImageView cardView2 = cty_list.findViewById(R.id.card2);
            cardView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.removeAllViews();
                }
            });
            ImageView cardView3 = cty_list.findViewById(R.id.card3);
            ImageView cardView4 = cty_list.findViewById(R.id.card4);
            ImageView cardView5 = cty_list.findViewById(R.id.card5);
            ImageView cardView6 = cty_list.findViewById(R.id.card6);
            new ImgBit(cardView).execute("https://files.tixget.com/attractions/thumb_horizontal/20180122150544889.jpg");
            new ImgBit(cardView2).execute("https://files.tixget.com/attractions/thumb_horizontal/201806051023526845.jpg");
            new ImgBit(cardView3).execute("https://files.tixget.com/attractions/thumb_horizontal/201801041429039751.png");
            new ImgBit(cardView4).execute("https://files.tixget.com/attractions/thumb_horizontal/201801250830587719.jpg");
            new ImgBit(cardView5).execute("https://files.tixget.com/attractions/thumb_horizontal/201801091702438650.png");
            new ImgBit(cardView6).execute("https://files.tixget.com/attractions/thumb_horizontal/20180319033326746.jpg");
            list_cty.addView(cty_list);
            page.setTarget("all");
        }else if(!"custom".equals(page.getTarget()) && page.getPage().equals("custom")){
            onclick_custom.setPaintFlags(onclick_custom.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            onclick_all.setPaintFlags(onclick_all.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
            onclick_nearby.setPaintFlags(onclick_nearby.getPaintFlags() & (~Paint.UNDERLINE_TEXT_FLAG));
            context.removeAllViews();
            View view = inflater.inflate(R.layout.form_custom_trip, context, false);
            context.addView(view);
            page.setTarget("custom");
        }
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("ยืนยัน");
        builder.setMessage("ต้องการออกจากระบบใช่หรือไม่ค่ะ ?");

        builder.setPositiveButton("ออกจากระบบ", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                //  Exit Application
                moveTaskToBack(true);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
