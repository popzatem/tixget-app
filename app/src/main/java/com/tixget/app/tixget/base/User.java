package com.tixget.app.tixget.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

public class User{
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mEditor;

    public User(Context context) {
        mPrefs = context.getSharedPreferences("profile_user", Context.MODE_PRIVATE);
        mEditor = mPrefs.edit();
    }

    public boolean Signin(String id, String data) {
        mEditor.putString("id", id);
        mEditor.putString("data", data);
        return mEditor.commit();
    }
    public boolean Signout() {
        mEditor.putString("id", "");
        return mEditor.commit();
    }

    public String getLng(){
        if ((!TextUtils.isEmpty(mPrefs.getString("lng", "")))) {
            return mPrefs.getString("lng", "");
        }
        return "en";
    }
    public String getFcy(){
        if ((!TextUtils.isEmpty(mPrefs.getString("fcy", "")))) {
            return mPrefs.getString("fcy", "");
        }
        return "thb";
    }
    public String getID(){
        return mPrefs.getString("id", "");
    }


    public String getData(){
        return mPrefs.getString("data", "");
    }

    public boolean setLng(String lng){
        mEditor.putString("lng", lng);
        return mEditor.commit();
    }
    public boolean setFcy(String lng){
        mEditor.putString("fcy", lng);
        return mEditor.commit();
    }
    public boolean check() {
        String id = mPrefs.getString("id", "");
        if ((!TextUtils.isEmpty(id))) {
            return true;
        }
        return false;
    }
}
