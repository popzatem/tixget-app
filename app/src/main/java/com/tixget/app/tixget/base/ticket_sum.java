package com.tixget.app.tixget.base;

public class ticket_sum {
    private int id;
    private int[][] ticket_id;
    private int[][] ticket_num;
    private int[][] ticket_total;
    private int[] total;
    public ticket_sum() {
    }

    public void setId(int id, int num){
        this.ticket_id[id][num] = num;
        this.ticket_num[id][num] = 0;
        this.ticket_total[id][num] = 0;
        this.total[id] = 0;
    }

    public void pNum(int id,int num,int price){
        this.ticket_num[id][num] = (this.ticket_num[id][num]+1);
        this.ticket_total[id][num] = (this.ticket_total[id][num]+price);
        this.total[id] = (this.total[id]+price);
    }

    public int[] getTotal(){
        return total;
    }

}
