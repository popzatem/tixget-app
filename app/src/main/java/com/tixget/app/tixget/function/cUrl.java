package com.tixget.app.tixget.function;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tixget.app.tixget.api.Attraction_api;
import com.tixget.app.tixget.base.Attraction;
import com.tixget.app.tixget.base.popular;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class cUrl{
    private String [] parameter;
    private Context mCtx;
    private RequestQueue queue;
    private JSONObject base;
    private final String TAG = getClass().getSimpleName();


    public cUrl(Context mCtx){
        this.queue = Volley.newRequestQueue(mCtx);
    }

    public void post(String url){
        try {
            URL urlc = new URL("https://api.tixget.com/th/thirdpartybooking/56/?un=RockStar&pa=Um9ja1N0YXI=");
            HttpsURLConnection con = (HttpsURLConnection)urlc.openConnection();
            String urlparam = "";
            con.setRequestMethod("POST");
            con.setRequestProperty("USER-AGENT", "Mozilla/5.0");
            con.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5");
            con.setDoInput(true);
            DataOutputStream dStream = new DataOutputStream(con.getOutputStream());
            dStream.writeBytes(urlparam);
            dStream.flush();
            dStream.close();
            int resCode = con.getResponseCode();
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line = "";
            StringBuilder res = new StringBuilder();
            while ((line = br.readLine()) != null){
                res.append(line);
            }
            br.close();
            Log.i("test", String.valueOf(resCode));
            Log.i("test", res.toString());
        } catch (IOException e) {
        e.printStackTrace();
    }

    }

    public void getAttraction(final Context mCtx, String url, final String id){
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject json = response.getJSONObject("items");
                            ContentValues values = new ContentValues();
                            values.put(com.tixget.app.tixget.base.Attraction.Column.ID_attc, id);
                            values.put(com.tixget.app.tixget.base.Attraction.Column.Data, json.toString());
                            Attraction_api attraction_api = new Attraction_api(mCtx);
                            attraction_api.insert(Attraction.TABLE, values, id);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", "Error");
                    }
                }
        );
        this.queue.add(getRequest);
    }

    public void getItems(final Context mCtx, String url){
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray json = response.getJSONArray("items");
                            Log.i("res-popular", json.getJSONObject(0).toString() + "  num " + json.length());
                            for (int i = 0 ; i < json.length(); i++){
                                ContentValues values = new ContentValues();
                                values.put(com.tixget.app.tixget.base.popular.Column.ID_attc, json.getJSONObject(i).getString("id"));
                                values.put(com.tixget.app.tixget.base.popular.Column.Name, json.getJSONObject(i).getString("name"));
                                values.put(com.tixget.app.tixget.base.popular.Column.thumbnail, json.getJSONObject(i).getString("thumbnail"));
                                values.put(com.tixget.app.tixget.base.popular.Column.normal_price, json.getJSONObject(i).getJSONObject("price").getString("normal_price"));
                                values.put(com.tixget.app.tixget.base.popular.Column.price, json.getJSONObject(i).getJSONObject("price").getString("price"));
                                values.put(com.tixget.app.tixget.base.popular.Column.fcy, json.getJSONObject(i).getString("fcy"));
                                DBHelper dbHelper = new DBHelper(mCtx);
                                dbHelper.insert(popular.TABLE,values);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", "Error");
                    }
                }
        );
        this.queue.add(getRequest);
    }

}
