package com.tixget.app.tixget;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;

import com.tixget.app.tixget.base.User;
import com.tixget.app.tixget.base.app;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    Context context = this;
    Locale myLocale;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int TIME = 1500;
        setContentView(R.layout.activity_main);
        app app = new app(this);
        app.Start(this,"1");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                User user = new User(context);
                if(user.check()){
                    Intent homeInstant = new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(homeInstant);
                    finish();
                }else{
                    Intent homeInstant = new Intent(MainActivity.this, SigninActivity.class);
                    startActivity(homeInstant);
                    finish();
                }
                myLocale = new Locale(user.getLng());
                Resources res = getResources();
                DisplayMetrics dm = res.getDisplayMetrics();
                Configuration conf = res.getConfiguration();
                conf.locale = myLocale;
                res.updateConfiguration(conf, dm);

            }
        }, TIME);
    }
}
