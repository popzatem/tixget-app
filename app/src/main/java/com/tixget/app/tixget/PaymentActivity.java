package com.tixget.app.tixget;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tixget.app.tixget.function.cUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PaymentActivity extends AppCompatActivity {
    TextView tDate,tTotal,tName,fFname,fLname,fEmail,fPhome,fAddress,fCity,fCountry,infoName,infoEmail,infoPhone,infoAdress,infoCity,infoCountry;
    Button bNext;
    LinearLayout pForm,information,payment,pCredit,pWallet,pLine;
    final Context mCtx = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        final Bundle base = getIntent().getExtras();
        ArrayList<List<String>> Ticket_list = (ArrayList<List<String>>) getIntent().getSerializableExtra("list");
        final ArrayList<String> booking = (ArrayList<String>) getIntent().getSerializableExtra("booking");
        LinearLayout ticket = (LinearLayout)findViewById(R.id.list);
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        for(int i = 0; i < Ticket_list.size(); i++){
            if(Integer.parseInt(Ticket_list.get(i).get(2)) > 0){
                View view = inflater.inflate(R.layout.list_tixget_attraction, ticket, false);
                TextView textView = view.findViewById(R.id.text_tixget);
                TextView normal_price = view.findViewById(R.id.normal_price);
                TextView textView28 = view.findViewById(R.id.textView28);
                TextView price = view.findViewById(R.id.price);
                textView28.setVisibility(View.GONE);
                normal_price.setText("x");
                price.setText(String.valueOf(Ticket_list.get(i).get(2))); //1 = id ,
                textView.setText(String.valueOf(Ticket_list.get(i).get(3)));
                ticket.addView(view);
            }
        }
        information = (LinearLayout)findViewById(R.id.information);
        pForm = (LinearLayout)findViewById(R.id.pForm);
        payment = (LinearLayout)findViewById(R.id.payment);

        bNext = (Button)findViewById(R.id.bNext);
        fFname = (TextView)findViewById(R.id.fFname);
        fLname = (TextView)findViewById(R.id.fLname);
        fEmail = (TextView)findViewById(R.id.fEmail);
        fPhome = (TextView)findViewById(R.id.fPhome);
        fAddress = (TextView)findViewById(R.id.fAddress);
        fCity = (TextView)findViewById(R.id.fCity);
        fCountry = (TextView)findViewById(R.id.fCountry);

        infoName = (TextView)findViewById(R.id.infoName);
        infoEmail = (TextView)findViewById(R.id.infoEmail);
        infoPhone = (TextView)findViewById(R.id.infoPhone);
        infoAdress = (TextView)findViewById(R.id.infoAdress);
        infoCity = (TextView)findViewById(R.id.infoCity);
        infoCountry = (TextView)findViewById(R.id.infoCountry);

        pCredit = (LinearLayout)findViewById(R.id.pCredit);
        pWallet = (LinearLayout)findViewById(R.id.pWallet);
        pLine = (LinearLayout)findViewById(R.id.pLine);

        tName = (TextView)findViewById(R.id.tName);
        tDate = (TextView)findViewById(R.id.cDate);
        tTotal = (TextView)findViewById(R.id.cTotal);
        tName.setText(base.getString("name"));
        tDate.setText(base.getString("date"));
        tTotal.setText(base.getString("total"));
        payment.setVisibility(View.GONE);
        information.setVisibility(View.GONE);

        pLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestQueue queue = Volley.newRequestQueue(mCtx);
                StringRequest sr = new StringRequest(Request.Method.POST,"https://api.tixget.com/th/thirdpartybooking/"+base.getString("id")+"/?un=RockStar&pa=Um9ja1N0YXI=", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);
                            if("200".equals(json.getString("status"))){
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.tixget.com/payment/linepay/?refno="+json.getString("items")));
                                startActivity(browserIntent);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("test", error.toString());

                    }
                }){
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    protected Map<String,String> getParams(){
                        Map<String, String>  params = new HashMap<String, String>();
                        String bk = TextUtils.join(", ", booking);
                        params.put("ticket", bk);
                        params.put("user_name", infoName.getText().toString());
                        params.put("user_email", infoEmail.getText().toString());
                        params.put("user_phone", infoPhone.getText().toString());
                        params.put("media", "");
                        params.put("ticket_date", base.getString("date"));
                        params.put("promocode", "");
                        params.put("rate_type", "");
                        params.put("address", infoAdress.getText().toString());
                        params.put("member_id", "");
                        params.put("city", infoCity.getText().toString());
                        params.put("country", infoCountry.getText().toString());
                        params.put("meeting", "");
                        params.put("timeslot", "");
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("Content-Type","application/x-www-form-urlencoded");
                        return params;
                    }
                };
                sr.setShouldCache(false);
                queue.add(sr);
            }
        });
        pWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestQueue queue = Volley.newRequestQueue(mCtx);
                StringRequest sr = new StringRequest(Request.Method.POST,"https://api.tixget.com/th/thirdpartybooking/"+base.getString("id")+"/?un=RockStar&pa=Um9ja1N0YXI=", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);
                            if("200".equals(json.getString("status"))){
                                Bundle b = new Bundle();
                                b.putString("url", "https://www.tixget.com/payment/alipay/?omise_refno="+json.getString("items"));
                                Intent instant = new Intent(PaymentActivity.this, WebviewActivity.class);
                                instant.putExtras(b);
                                startActivity(instant);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("test", error.toString());

                    }
                }){
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    protected Map<String,String> getParams(){
                        Map<String, String>  params = new HashMap<String, String>();
                        String bk = TextUtils.join(", ", booking);
                        params.put("ticket", bk);
                        params.put("user_name", infoName.getText().toString());
                        params.put("user_email", infoEmail.getText().toString());
                        params.put("user_phone", infoPhone.getText().toString());
                        params.put("media", "");
                        params.put("ticket_date", base.getString("date"));
                        params.put("promocode", "");
                        params.put("rate_type", "");
                        params.put("address", infoAdress.getText().toString());
                        params.put("member_id", "");
                        params.put("city", infoCity.getText().toString());
                        params.put("country", infoCountry.getText().toString());
                        params.put("meeting", "");
                        params.put("timeslot", "");
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("Content-Type","application/x-www-form-urlencoded");
                        return params;
                    }
                };
                sr.setShouldCache(false);
                queue.add(sr);
            }
        });
        pCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestQueue queue = Volley.newRequestQueue(mCtx);
                StringRequest sr = new StringRequest(Request.Method.POST,"https://api.tixget.com/th/thirdpartybooking/"+base.getString("id")+"/?un=RockStar&pa=Um9ja1N0YXI=", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);
                            if("200".equals(json.getString("status"))){
//                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.tixget.com/home/th/gateway/?media=&fcy=cny&refno="+json.getString("items")));
//                                startActivity(browserIntent);
//                                finish();
                                Bundle b = new Bundle();
                                b.putString("url", "https://www.tixget.com/home/th/gateway/?media=&fcy=thb&refno="+json.getString("items"));
                                Intent instant = new Intent(PaymentActivity.this, WebviewActivity.class);
                                instant.putExtras(b);
                                startActivity(instant);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("test", error.toString());

                    }
                }){
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    protected Map<String,String> getParams(){
                        Map<String, String>  params = new HashMap<String, String>();
                        String bk = TextUtils.join(", ", booking);
                        params.put("ticket", bk);
                        params.put("user_name", infoName.getText().toString());
                        params.put("user_email", infoEmail.getText().toString());
                        params.put("user_phone", infoPhone.getText().toString());
                        params.put("media", "");
                        params.put("ticket_date", base.getString("date"));
                        params.put("promocode", "");
                        params.put("rate_type", "");
                        params.put("address", infoAdress.getText().toString());
                        params.put("member_id", "");
                        params.put("city", infoCity.getText().toString());
                        params.put("country", infoCountry.getText().toString());
                        params.put("meeting", "");
                        params.put("timeslot", "");
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("Content-Type","application/x-www-form-urlencoded");
                        return params;
                    }
                };
                sr.setShouldCache(false);
                queue.add(sr);
            }

        });

        fCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertCountry();
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });
        fCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertCity();
            }
        });
        bNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                infoName.setText(fFname.getText() + " " + fLname.getText());
                infoEmail.setText(fEmail.getText());
                infoPhone.setText(fPhome.getText());
                infoAdress.setText(fAddress.getText());
                infoCity.setText(fCity.getText());
                infoCountry.setText(fCountry.getText());


                pForm.setVisibility(View.GONE);
                information.setVisibility(View.VISIBLE);
                payment.setVisibility(View.VISIBLE);

            }
        });
        //test.setText(strings.substring(3));
    }
    public void alertCity(){
        final String[] colors = {"red", "green", "blue", "black"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pick a color");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fCity.setText(colors[which]);
            }
        });
        builder.show();
    }
    public void alertCountry(){
        final String[] Country = {"Afghanistan","Aland Islands","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antarctica","Antigua and Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bonaire, Saint Eustatius and Saba ","Bosnia and Herzegovina","Botswana","Bouvet Island","Brazil","British Indian Ocean Territory","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central African Republic","Chad","Chile","China","Christmas Island","Cocos Islands","Colombia","Comoros","Cook Islands","Costa Rica","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Democratic Republic of the Congo","Denmark","Djibouti","Dominica","Dominican Republic","East Timor","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Guiana","French Polynesia","French Southern Territories","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guernsey","Guinea","Guinea-Bissau","Guyana","Haiti","Heard Island and McDonald Islands","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Ivory Coast","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macao","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique","Mauritania","Mauritius","Mayotte","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Niue","Norfolk Island","North Korea","Northern Mariana Islands","Norway","Oman","Pakistan","Palau","Palestinian Territory","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Pitcairn","Poland","Portugal","Puerto Rico","Qatar","Republic of the Congo","Reunion","Romania","Russia","Rwanda","Saint Barthelemy","Saint Helena","Saint Kitts and Nevis","Saint Lucia","Saint Martin","Saint Pierre and Miquelon","Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Sint Maarten","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Georgia and the South Sandwich Islands","South Korea","South Sudan","Spain","Sri Lanka","Sudan","Suriname","Svalbard and Jan Mayen","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Togo","Tokelau","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Turks and Caicos Islands","Tuvalu","U.S. Virgin Islands","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","United States Minor Outlying Islands","Uruguay","Uzbekistan","Vanuatu","Vatican","Venezuela","Vietnam","Wallis and Futuna","Western Sahara","Yemen","Zambia","Zimbabwe"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Coumtry");
        builder.setItems(Country, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fCountry.setText(Country[which]);
            }
        });
        builder.show();
    }
}
