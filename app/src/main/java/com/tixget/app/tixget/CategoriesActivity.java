package com.tixget.app.tixget;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.flexbox.FlexboxLayout;
import com.tixget.app.tixget.base.User;
import com.tixget.app.tixget.function.ImgBit;
import com.tixget.app.tixget.function.cUrl;
import com.tixget.app.tixget.function.function;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class CategoriesActivity extends AppCompatActivity {
    private User user;
    LayoutInflater inflater;
    FlexboxLayout  flexboxLayout;
    Context mCtx = this;
    private DecimalFormat formatter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        user = new User(this);
        final Bundle base = getIntent().getExtras();
        TextView name = (TextView)findViewById(R.id.name);
        name.setText(base.getString("name"));
        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        flexboxLayout = (FlexboxLayout)findViewById(R.id.context);
        inflater = LayoutInflater.from(this);
        formatter = new DecimalFormat("#,###,###,###");
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest sr = new StringRequest(Request.Method.GET,"http://ec2-13-228-251-38.ap-southeast-1.compute.amazonaws.com:3000/attractionslist/"+user.getLng()+"/"+base.getString("id"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    final JSONArray array = new JSONArray(response);
                    for(int x = 0; x < array.length(); x++){
                        View view = inflater.inflate(R.layout.frag_categories, flexboxLayout, false);
                        CardView cardView = view.findViewById(R.id.card);
                        ImageView imageView = view.findViewById(R.id.img);
                        TextView city = view.findViewById(R.id.City);
                        final TextView textView = view.findViewById(R.id.name);
                        TextView textViewe = view.findViewById(R.id.normal_price);
                        TextView price = view.findViewById(R.id.price);
                        ImageView sharing = view.findViewById(R.id.sharing);
                        textView.setText(array.getJSONObject(x).getString("name"));
                        price.setText(array.getJSONObject(x).getJSONObject("price").getString("price"));
                        textViewe.setText(array.getJSONObject(x).getJSONObject("price").getString("normal_price"));
                        city.setText(array.getJSONObject(x).getString("city_name"));

                        if(!array.getJSONObject(x).getJSONObject("price").getString("price").equals("null")){
                            price.setText(function.LangCode(user.getFcy())+" "+formatter.format(Integer.parseInt(array.getJSONObject(x).getJSONObject("price").getString("price"))));
                        }
                        if(!array.getJSONObject(x).getJSONObject("price").getString("normal_price").equals("-") && !array.getJSONObject(x).getJSONObject("price").getString("normal_price").equals("null") ){
                            textViewe.setText(function.LangCode(user.getFcy())+" "+formatter.format(Integer.parseInt(array.getJSONObject(x).getJSONObject("price").getString("normal_price"))));
                        }
                        textViewe.setPaintFlags(textViewe.getPaintFlags() | Paint.DEV_KERN_TEXT_FLAG);

                        final String Id = array.getJSONObject(x).getString("id");
                        sharing.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String name = (String) textView.getText();
                                String link = new String("https://www.tixget.com/home/"+user.getLng()+"/detail/"+Id+"/"+name.replace(' ','-')+"/?media=&fcy="+user.getFcy());
                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
                                sendIntent.putExtra(Intent.EXTRA_TEXT, link);
                                sendIntent.setType("text/plain");
                                startActivity(sendIntent);
                            }
                        });

                        new ImgBit(imageView).execute(array.getJSONObject(x).getString("thumbnail"));
                        flexboxLayout.addView(view);
                        final int finalX = x;
                        cardView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle b = new Bundle();
                                try {
                                    b.putString("base", array.getJSONObject(finalX).getString("id"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Intent instant = new Intent(CategoriesActivity.this, AttractionActivity.class);
                                instant.putExtras(b);
                                startActivity(instant);
                                cUrl cUrl = new cUrl(mCtx);
                                try {
                                    cUrl.getAttraction(mCtx,"https://api.tixget.com/"+user.getLng()+"/attraction/"+array.getJSONObject(finalX).getString("id")+"/?un=RockStar&pa=Um9ja1N0YXI=&fcy=thb", array.getJSONObject(finalX).getString("id"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        queue.add(sr);
    }
}
