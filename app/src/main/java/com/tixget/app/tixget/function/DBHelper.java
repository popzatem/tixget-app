package com.tixget.app.tixget.function;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.tixget.app.tixget.base.popular;

public class DBHelper extends SQLiteOpenHelper {
    private final String TAG = getClass().getSimpleName();
    private SQLiteDatabase sqLiteDatabase;

    public DBHelper(Context context) {
        super(context, popular.DATABASE_NAME, null, popular.DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_FRIEND_TABLE = String.format("CREATE TABLE %s " +
                        "(%s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT)",
                popular.TABLE,
                popular.Column.ID_attc,
                popular.Column.normal_price,
                popular.Column.thumbnail,
                popular.Column.price,
                popular.Column.Name,
                popular.Column.fcy);
        Log.i(TAG, CREATE_FRIEND_TABLE);
        db.execSQL(CREATE_FRIEND_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String DROP_FRIEND_TABLE = "DROP TABLE IF EXISTS " + popular.TABLE;

        db.execSQL(DROP_FRIEND_TABLE);

        Log.i(TAG, "Upgrade Database from " + oldVersion + " to " + newVersion);

        onCreate(db);
    }
    public void StartApp(String table,ContentValues values) {
        sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.insert(table, null, values);
        sqLiteDatabase.close();
    }

    public void insert(String table,ContentValues values) {
        sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.insert(table, null, values);
        sqLiteDatabase.close();
    }

    public Cursor queryAll(String table) {
        sqLiteDatabase = this.getWritableDatabase();
        String QUERY_ALL_FRIEND = "SELECT * FROM " + table;
        Cursor cursor = sqLiteDatabase.rawQuery(QUERY_ALL_FRIEND, null);
        cursor.moveToFirst();
        sqLiteDatabase.close();
        return cursor;
    }
}