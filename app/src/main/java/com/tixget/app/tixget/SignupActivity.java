package com.tixget.app.tixget;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tixget.app.tixget.base.User;
import com.tixget.app.tixget.function.function;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends AppCompatActivity {
    TextView SignupFNinput,SignupLNinput,SignupEmailinput,SignupPassinput,SignupCPassinput,alert0,alert1;
    Button button;
    Context context = this;
    boolean c;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        SignupFNinput = (TextView)findViewById(R.id.SignupFNinput);
        SignupLNinput = (TextView)findViewById(R.id.SignupLNinput);
        SignupEmailinput = (TextView)findViewById(R.id.SignupEmailinput);
        SignupPassinput = (TextView)findViewById(R.id.SignupPassinput);
        SignupCPassinput = (TextView)findViewById(R.id.SignupCPassinput);

        alert0 = (TextView)findViewById(R.id.alert0);
        alert1 = (TextView)findViewById(R.id.alert1);
        alert0.setVisibility(View.GONE);
        alert1.setVisibility(View.GONE);
        button = (Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SignupPassinput.getText().length() >= 6){
                    if(SignupPassinput.getText().toString().equals(SignupCPassinput.getText().toString())){
                        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        String url = "https://api.tixget.com/en/register/?un=RockStar&pa=Um9ja1N0YXI";
                        RequestQueue queue = Volley.newRequestQueue(context);
                        StringRequest sr = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject json = new JSONObject(response);
                                    Log.i("test", json.toString());
                                    if("200".equals(json.getString("status"))){
                                        alertDialog.setTitle("Success");
                                        alertDialog.setMessage("Registration process is successful.");
                                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                        alertDialog.show();
                                        finish();
                                    }else{
                                        alertDialog.setTitle("Error");
                                        alertDialog.setMessage("unable to connect to server");
                                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                        alertDialog.show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }){
                            @Override
                            public String getBodyContentType() {
                                return "application/json; charset=utf-8";
                            }

                            @RequiresApi(api = Build.VERSION_CODES.O)
                            @Override
                            protected Map<String,String> getParams(){
                                Map<String, String>  params = new HashMap<String, String>();
                                params.put("title", "null");
                                params.put("email", SignupEmailinput.getText().toString());
                                params.put("password", SignupPassinput.getText().toString());
                                params.put("firstname", SignupFNinput.getText().toString());
                                params.put("lastname", SignupLNinput.getText().toString());
                                params.put("type", "0");
                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String,String> params = new HashMap<String, String>();
                                params.put("Content-Type","application/x-www-form-urlencoded");
                                return params;
                            }
                        };
                        sr.setShouldCache(false);
                        queue.add(sr);
                    }else {
                        alert1.setVisibility(View.VISIBLE);
                        return;
                    }
                }else{
                    alert0.setVisibility(View.VISIBLE);
                    return;
                }
            }
        });
        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
