package com.tixget.app.tixget.function;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.InputStream;
import java.net.URL;

public class BgBit extends AsyncTask<String, Void, BitmapDrawable> {
    CardView imageView;

    public BgBit(CardView imageView){
        this.imageView = imageView;
    }

    /*
        doInBackground(Params... params)
            Override this method to perform a computation on a background thread.
     */
    protected BitmapDrawable doInBackground(String...urls){
        String urlOfImage = urls[0];
        Bitmap logo = null;
        BitmapDrawable bg = null;
        try{
            InputStream is = new URL(urlOfImage).openStream();
                /*
                    decodeStream(InputStream is)
                        Decode an input stream into a bitmap.
                 */
            logo = BitmapFactory.decodeStream(is);
            bg = new BitmapDrawable(logo);
        }catch(Exception e){ // Catch the download exception
            e.printStackTrace();
        }
        return bg;
    }

    /*
        onPostExecute(Result result)
            Runs on the UI thread after doInBackground(Params...).
     */
    protected void onPostExecute(BitmapDrawable result){
        imageView.setBackgroundDrawable(result);
    }

}