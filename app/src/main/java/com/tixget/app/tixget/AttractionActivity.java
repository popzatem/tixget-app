package com.tixget.app.tixget;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Paint;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tixget.app.tixget.api.Attraction_api;
import com.tixget.app.tixget.base.User;
import com.tixget.app.tixget.function.BgBit;
import com.tixget.app.tixget.function.ImgBit;
import com.tixget.app.tixget.function.function;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class AttractionActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener {
    private GoogleMap mMap;
    private TextView Comment;
    private User user;
    private LayoutInflater inflater;
    private Context mCtx = this;
    private RequestQueue queue;
    private Bundle base;
    private LinearLayout list_review;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attraction);
        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        base = getIntent().getExtras();
        Comment = (TextView)findViewById(R.id.Comment);
        Log.i("res", base.getString("base"));
        user = new User(this);
        list_review = (LinearLayout)findViewById(R.id.list_review);
        inflater = LayoutInflater.from(mCtx);
        queue = Volley.newRequestQueue(mCtx);
        try {
            JSONObject u = new JSONObject(user.getData());
            TextView Name = (TextView)findViewById(R.id.Name);
            ImageView avater = (ImageView)findViewById(R.id.avater);
            new ImgBit(avater).execute(u.getString("img"));
            Name.setText(u.getString("firstname")+ " "+ u.getString("lastname"));
            Button BReview = (Button)findViewById(R.id.BReview);
            BReview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(TextUtils.isEmpty(Comment.getText().toString())){
                        Toast.makeText(getApplicationContext(), "Comment Review", 0).show();
                    }else{
                        String url = "https://api.tixget.com/th/review/"+base.getString("base")+"/?un=RockStar&pa=Um9ja1N0YXI=";
                        StringRequest sr = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                LoadReview();
                                Comment.setText("");
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                            }
                        }){
                            @Override
                            public String getBodyContentType() {
                                return "application/json; charset=utf-8";
                            }

                            @RequiresApi(api = Build.VERSION_CODES.O)
                            @Override
                            protected Map<String,String> getParams(){
                                Map<String, String>  params = new HashMap<String, String>();
                                params.put("member_id", user.getID());
                                params.put("review", Comment.getText().toString());
                                params.put("star", "5");
                                params.put("form", "2");
                                params.put("redir", "");
                                return params;
                            }
                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String,String> params = new HashMap<String, String>();
                                params.put("Content-Type","application/x-www-form-urlencoded");
                                return params;
                            }
                        };
                        sr.setShouldCache(false);
                        queue.add(sr);
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest sr = new StringRequest(Request.Method.GET,"http://ec2-13-228-251-38.ap-southeast-1.compute.amazonaws.com:3000/attraction/"+user.getLng()+"/"+base.getString("base"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray array = new JSONArray(response);
                    TextView title = (TextView)findViewById(R.id.title);
                    JSONObject json =  array.getJSONObject(0);
                    title.setText(array.getJSONObject(0).getString("name"));
                    JSONArray ticket = json.getJSONArray("ticket");
                    RatingBar ratingBar = findViewById(R.id.rating);
                    ImageView img = (ImageView)findViewById(R.id.Img);
                    new ImgBit(img).execute(json.getString("thumb_horizontal"));
                    LinearLayout tixget = (LinearLayout)findViewById(R.id.tixget_box);

                    DecimalFormat formatter = new DecimalFormat("#,###,###,###");
                    for(int x = 0; x < ticket.length(); x++){
                            View view = inflater.inflate(R.layout.list_tixget_attraction, tixget, false);
                            TextView textView = view.findViewById(R.id.text_tixget);
                            TextView normal_price = view.findViewById(R.id.normal_price);
                            TextView price = view.findViewById(R.id.price);
                            textView.setText(ticket.getJSONObject(x).getString("name"));
                            price.setText(function.LangCode(user.getFcy())+""+ticket.getJSONObject(x).getString("price"));
                            normal_price.setText(function.LangCode(user.getFcy())+""+ticket.getJSONObject(x).getString("normal_price"));
                            normal_price.setPaintFlags(normal_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            tixget.addView(view);
                    }
                    JSONArray optional = json.getJSONArray("optional");
                    WebView con_highlight = (WebView) findViewById(R.id.con_highlight);
                    WebView con_description = (WebView) findViewById(R.id.con_description);
                    WebView con_itinerary = (WebView) findViewById(R.id.con_itinerary);
                    WebView con_what_includes = (WebView) findViewById(R.id.con_what_includes);

                    con_highlight.loadDataWithBaseURL(null, optional.getJSONObject(0).getString("highlight"), "text/html", "utf-8", null);
                    con_description.loadDataWithBaseURL(null, optional.getJSONObject(0).getString("description"), "text/html", "utf-8", null);
                    con_itinerary.loadDataWithBaseURL(null, optional.getJSONObject(0).getString("itinerary"), "text/html", "utf-8", null);
                    con_what_includes.loadDataWithBaseURL(null, optional.getJSONObject(0).getString("what_includes"), "text/html", "utf-8", null);

                    Button button = (Button)findViewById(R.id.attk_booking);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent instant = new Intent(AttractionActivity.this, BookingActivity.class);
                            Bundle b = new Bundle();
                            b.putString("base", base.getString("base"));
                            instant.putExtras(b);
                            startActivity(instant);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        queue.add(sr);
        LoadReview();
    }

    public void LoadReview(){
        list_review.removeAllViews();
        StringRequest sr_review = new StringRequest(Request.Method.GET,"https://api.tixget.com/en/attractionreview/"+base.getString("base")+"/?un=RockStar&pa=Um9ja1N0YXI=", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject j = new JSONObject(response);
                    LinearLayout lReview = (LinearLayout)findViewById(R.id.lReview);
                    if(j.getString("status").equals("404")){lReview.setVisibility(View.GONE);}
                    JSONArray array = j.getJSONArray("items");
                    Log.i("test", String.valueOf(array.length()));

                    int num = array.length();
                    for (int x = 0 ;x < num;x++){
                        View review = inflater.inflate(R.layout.list_review, list_review, false);
                        ImageView avater = review.findViewById(R.id.avater);
                        new ImgBit(avater).execute(array.getJSONObject(x).getString("user_img"));
                        TextView name = review.findViewById(R.id.name);
                        TextView comment = review.findViewById(R.id.comment);
                        RatingBar ratingBar = review.findViewById(R.id.ratingBar);
                        ratingBar.setRating(Integer.parseInt(array.getJSONObject(x).getString("star")));
                        name.setText(array.getJSONObject(x).getString("user_name"));
                        comment.setText(array.getJSONObject(x).getString("review"));
                        list_review.addView(review);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        sr_review.setShouldCache(false);
        queue.add(sr_review);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney, Australia, and move the camera.
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMarkerClickListener(this);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        startActivity(new Intent(AttractionActivity.this, MapsActivity.class));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        startActivity(new Intent(AttractionActivity.this, MapsActivity.class));
        return false;
    }
}
