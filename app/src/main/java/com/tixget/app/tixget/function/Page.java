package com.tixget.app.tixget.function;

public class Page {
    private String page,target;

    public Page(String page, String target) {
        this.page = page;
        this.target = target;
    }

    public String getPage() {
        return page;
    }
    public void setPage(String page) {
        this.page = page;
    }
    public String getTarget() {
        return target;
    }
    public void setTarget(String target) {
        this.target = target;
    }
}
