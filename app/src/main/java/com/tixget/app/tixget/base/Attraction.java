package com.tixget.app.tixget.base;


import org.json.JSONObject;

public class Attraction {
    public static final String DATABASE_NAME = "base_Attraction.db";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE = "Attraction";
    public static final String TABLE2 = "optional";
    private int id;
    private JSONObject data;

    public Attraction(){

    }
    public class Column {
        public static final String ID_attc = "id_attc";
        public static final String Data = "data";
    }
}
