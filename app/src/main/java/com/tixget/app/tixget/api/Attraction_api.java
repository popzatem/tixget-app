package com.tixget.app.tixget.api;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.tixget.app.tixget.base.Attraction;

public class Attraction_api extends SQLiteOpenHelper {
    private final String TAG = getClass().getSimpleName();
    private SQLiteDatabase sqLiteDatabase;

    public Attraction_api(Context context) {
        super(context, Attraction.DATABASE_NAME, null, Attraction.DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_FRIEND_TABLE = String.format("CREATE TABLE %s " +
                        "(%s TEXT, %s TEXT)",
                Attraction.TABLE,
                Attraction.Column.ID_attc,
                Attraction.Column.Data);
        Log.i(TAG, CREATE_FRIEND_TABLE);
        db.execSQL(CREATE_FRIEND_TABLE);


    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_FRIEND_TABLE = "DROP TABLE IF EXISTS " + Attraction.TABLE;
        db.execSQL(DROP_FRIEND_TABLE);
        Log.i(TAG, "Upgrade Database from " + oldVersion + " to " + newVersion);
        onCreate(db);
    }

    public void insert(String table,ContentValues values, String id) {
        sqLiteDatabase = this.getWritableDatabase();
        String Query = "Select * from "+table+" where "+Attraction.Column.ID_attc+" = " + id;
        Cursor cursor = sqLiteDatabase.rawQuery(Query, null);
        if(cursor.getCount() <= 0) {
            sqLiteDatabase.insert(table, null, values);
            Log.i("res-attc","Save"+ cursor.moveToFirst());
        }else{
            int row = sqLiteDatabase.update(table,
                    values,
                    Attraction.Column.ID_attc + " = ? ",
                    new String[] { String.valueOf(id) });
            Log.i("res-attc","update" + cursor.moveToFirst());
        }
        sqLiteDatabase.close();
    }

    public Cursor getID(String id) {
        sqLiteDatabase = this.getWritableDatabase();
        String Query = "Select * from "+Attraction.TABLE+" where "+Attraction.Column.ID_attc+" = " + id;
        Cursor cursor = sqLiteDatabase.rawQuery(Query, null);
        cursor.moveToFirst();
        sqLiteDatabase.close();
        return cursor;
    }


}
