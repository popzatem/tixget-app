package com.tixget.app.tixget;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tixget.app.tixget.base.User;
import com.tixget.app.tixget.function.function;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SigninActivity extends AppCompatActivity {
    private JSONObject jsonRequest;
    private EditText eusername;
    private EditText epassword;
    private Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        eusername = (EditText) findViewById(R.id.username);
        epassword = (EditText) findViewById(R.id.password);
        Button button = (Button) findViewById(R.id.btn_login);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
        TextView textsignup = (TextView) findViewById(R.id.btn_signup);
        textsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SigninActivity.this, SignupActivity.class));
            }
        });
        textsignup.setPaintFlags(textsignup.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    public void login() {
        final AlertDialog alertDialog = new AlertDialog.Builder(SigninActivity.this).create();
        Log.i("log", String.valueOf(eusername.getText())+ " | "+ function.MD5("0828941636"));
        if(TextUtils.isEmpty(eusername.getText().toString()) || TextUtils.isEmpty(epassword.getText().toString())){
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("กรุณาระบุบข้อมูลให้ครบถ้วน");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            return;
        }else{
            String url = "https://api.tixget.com/th/login/?un=RockStar&pa=Um9ja1N0YXI=";
            RequestQueue queue = Volley.newRequestQueue(this);
            StringRequest sr = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject json = new JSONObject(response);
                        if("200".equals(json.getString("status"))){
                            User user = new User(context);
                            user.Signin(json.getJSONObject("items").getString("id").toString(), json.getJSONObject("items").toString());
                            Intent instant = new Intent(SigninActivity.this, HomeActivity.class);
                            startActivity(instant);
                            finish();
                        }else{
                            alertDialog.setTitle("Alert");
                            alertDialog.setMessage("Email and Password");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("test", error.toString());

                }
            }){
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                protected Map<String,String> getParams(){
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("email", eusername.getText().toString());
                    params.put("password", function.MD5(epassword.getText().toString()));
                    params.put("type", "0");
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Content-Type","application/x-www-form-urlencoded");
                    return params;
                }
            };
            sr.setShouldCache(false);
            queue.add(sr);
        }
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("ยืนยัน");
        builder.setMessage("ต้องการออกจากระบบใช่หรือไม่ค่ะ ?");

        builder.setPositiveButton("ออกจากระบบ", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                //  Exit Application
                moveTaskToBack(true);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
