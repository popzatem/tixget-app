package com.tixget.app.tixget;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tixget.app.tixget.base.User;
import com.tixget.app.tixget.function.ImgBit;
import com.tixget.app.tixget.function.function;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EditprofileActivity extends AppCompatActivity {
    ImageView iImg;
    User user;
    String password;
    EditText iName,iEmail,iPhone,iAdress,iCity,iCountry,iPass,iConpass;
    TextView textView38;
    RequestQueue queue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        user = new User(this);
        iImg = (ImageView)findViewById(R.id.iImg);
        queue = Volley.newRequestQueue(this);
        try {
            JSONObject u = new JSONObject(user.getData());
            new ImgBit(iImg).execute(u.getString("img"));
            Log.i("test", u.getString("img"));

            iName = (EditText)findViewById(R.id.iName);
            iEmail = (EditText)findViewById(R.id.iEmail);
            iPhone = (EditText)findViewById(R.id.iPhone);
            iAdress = (EditText)findViewById(R.id.iAdress);
            iCity = (EditText)findViewById(R.id.iCity);
            iCountry = (EditText)findViewById(R.id.iCountry);
            textView38 = (TextView)findViewById(R.id.textView38);
            iEmail.setEnabled(false);



            String url = "http://ec2-13-228-251-38.ap-southeast-1.compute.amazonaws.com:3000/auth/profile/" + u.getString("id");
            StringRequest sr = new StringRequest(Request.Method.GET,url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject json = new JSONObject(response);
                        Log.i("test" , json.toString());

                        iName.setText(json.getString("firstname")+ " " + json.getString("lastname"));
                        iEmail.setText(json.getString("email"));
                        iPhone.setText(json.getString("phone_code") + "" + json.getString("phone"));
                        password = json.getString("password");
                        if (json.getString("address").equals(null)) {
                            iAdress.setText(json.getString("address"));
                        }
                        if (json.getString("city").equals(null)) {
                            iCity.setText(json.getString("city"));
                        }
                        if (json.getString("country").equals(null) ) {
                            iCountry.setText(json.getString("country"));
                        }
                        textView38.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Save();
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("test", error.toString());

                }
            });
            sr.setShouldCache(false);
            queue.add(sr);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public void Save() {
        iPass = (EditText)findViewById(R.id.iPass);
        iConpass = (EditText)findViewById(R.id.iConpass);
        String cPass = function.MD5(iPass.getText().toString());
        if(TextUtils.isEmpty(iPass.getText())){
            Toast.makeText(getApplicationContext(), "กรุณาระบุบรหัสผ่าน", 0).show();
        }else if(!iPass.getText().toString().equals(iConpass.getText().toString())){
            Toast.makeText(getApplicationContext(), "หรัสผ่านไม่ตรงกัน", 0).show();
        }else if(cPass.equals(password)){
            String url2 = "http://localhost:3000/auth/profile_update/";
            StringRequest sr2 = new StringRequest(Request.Method.POST, url2, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(getApplicationContext(), "update", 0).show();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("test", "Error : " + error.toString());
                }
            }){
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                protected Map<String,String> getParams(){
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("id", user.getID());
                    params.put("name", iName.getText().toString());
                    params.put("address", iAdress.getText().toString());
                    params.put("city", iCity.getText().toString());
                    params.put("country", iCountry.getText().toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Content-Type","application/x-www-form-urlencoded");
                    return params;
                }
            };
            sr2.setShouldCache(false);
            queue.add(sr2);
        }else{
            Toast.makeText(getApplicationContext(), "รหัสผ่านไม่ถุกต้อง", 0).show();
        }
    }
}
