package com.tixget.app.tixget;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tixget.app.tixget.function.ImgBit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TicketsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tickets);
        final Bundle base = getIntent().getExtras();
        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        final LinearLayout content = (LinearLayout)findViewById(R.id.content);
        final LayoutInflater inflater = LayoutInflater.from(this);
        StringRequest sr = new StringRequest(Request.Method.GET,"https://api.tixget.com/th/ticket/?un=RockStar&pa=Um9ja1N0YXI=&refno="+base.getString("refno"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    if("200".equals(json.getString("status"))){
                        TextView name = (TextView)findViewById(R.id.name);
                        name.setText(json.getJSONObject("items").getString("attraction_name"));
                        for(int x = 0;x < json.getJSONObject("items").getJSONArray("ticket").length();x++){
                            View view = inflater.inflate(R.layout.list_ticket, content, false);
                            TextView myname = view.findViewById(R.id.myname);
                            TextView mydate = view.findViewById(R.id.mydate);
                            TextView myid = view.findViewById(R.id.myid);
                            TextView tname = view.findViewById(R.id.tname);
                            ImageView ticket = view.findViewById(R.id.ticket);

                            tname.setText(json.getJSONObject("items").getString("attraction_name"));
                            myname.setText(json.getJSONObject("items").getJSONArray("ticket").getJSONObject(x).getString("name"));
                            mydate.setText(json.getJSONObject("items").getString("ticket_date"));
                            myid.setText(json.getJSONObject("items").getJSONArray("ticket").getJSONObject(x).getString("booking_code"));
                            new ImgBit(ticket).execute(json.getJSONObject("items").getJSONArray("ticket").getJSONObject(x).getString("img"));
                            content.addView(view);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        queue.add(sr);
    }
}
