package com.tixget.app.tixget;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tixget.app.tixget.base.User;
import com.tixget.app.tixget.function.ImgBit;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileActivity extends AppCompatActivity {
    TextView name,email,phone;
    User user;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        user = new User(this);
        name = (TextView)findViewById(R.id.name);
        email= (TextView)findViewById(R.id.email);
        phone = (TextView)findViewById(R.id.phone);
        LinearLayout myticket = (LinearLayout)findViewById(R.id.my_ticket);
        LinearLayout logout = (LinearLayout)findViewById(R.id.logout);
        ImageView avater = (ImageView)findViewById(R.id.avater);

        myticket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this,MyticketsActivity.class));
            }
        });
        try {
            JSONObject u = new JSONObject(user.getData());
            name.setText(u.getString("firstname")+ " "+ u.getString("lastname"));
            email.setText(u.getString("email"));
            new ImgBit(avater).execute(u.getString("img"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        TextView edit = (TextView)findViewById(R.id.edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this,EditprofileActivity.class));
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.Signout();
                Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }
}
