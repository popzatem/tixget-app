package com.tixget.app.tixget;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.tixget.app.tixget.function.Page;
import com.tixget.app.tixget.function.function;

public class fragment_home extends Fragment {
    private TextView onclick_all,onclick_custom,onclick_nearby;
    @Override
    public View onCreateView(LayoutInflater mInflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = mInflater.inflate(R.layout.activity_home, container, false);
        final Page page = new Page("all",null);
        ImageView menu = view.findViewById(R.id.menu);
        BottomNavigationView bottomNavigationView = view.findViewById(R.id.bottom_nav_view);
        function.removeNavigationShiftMode(bottomNavigationView);
        EditText search = view.findViewById(R.id.search);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_recent:
                        // do this event
                        return true;
                    case R.id.item_favorite:
                        // do this event
                        return true;
                    case R.id.item_nearby:
                        // do this event
                        return true;
                    case R.id.item_test:
                        // do this event
                        return true;
                }
                return false;
            }
        });

        onclick_all = view.findViewById(R.id.onclick_custon_all);
        onclick_custom = view.findViewById(R.id.onclick_custom_home);
        onclick_nearby = view.findViewById(R.id.onclick_custon_nearby);
        onclick_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        onclick_custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return view;
    }
}
